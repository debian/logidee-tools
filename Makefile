# $Id$
# Raphaël Hertzog, Logidée 2000-2001
# See LICENSE file for the copyright notice
# Copie le Makefile générique dans tous les répertoires XML

REP=doc xml

doc: 

makefiles: 
	@for i in $(REP) ; do \
		echo Creating Makefile in $$i ; \
		cp Makefile.generic $$i/Makefile ; \
		if test -r $$i/Makefile.local ; then \
			perl -pi -e "s|^#include Makefile.local$$|include Makefile.local|" $$i/Makefile ; \
		fi ; \
	done

cleanmakefiles:
	@for i in $(REP) ; do \
		echo Removing Makefile in $$i ; \
		rm -f $$i/Makefile ; \
	done

catalog: catalogue.xml xsl/catalogue.xsl
	test -d catalog || mkdir catalog
	xsltproc --param dir \'$(PWD)\' \
		 --param odir \'$(PWD)/catalog\' \
	    xsl/catalogue.xsl catalogue.xml >catalog/index.html
	cp charte/default/default.css catalog/

%:
	@for i in $(REP) ; do \
		$(MAKE) -C $$i $@ ; \
	done

.PHONY: makefiles cleanmakefiles catalog %
