<?xml version="1.0" encoding="UTF-8" ?>
<!--
# Stéphane Casset, Raphaël Hertzog, Logidée 2000-2001
# See LICENSE file for copyright notice
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:param name="selection" select="'all'"/>
<xsl:param name="trainer" select="'false'"/>
<xsl:param name="slide" select="'true'"/>
<xsl:param name="description" select="'true'"/>

<xsl:output method="text" encoding="UTF-8"/>

<xsl:strip-space elements="title table row col"/>
<xsl:preserve-space elements="code"/>

<xsl:include href="charte.xsl"/>
<xsl:include href="tex.xsl"/>


<!-- The module -->
<xsl:template match="module">
  <xsl:call-template name="begindoc"/>
  <xsl:apply-templates/>
  <xsl:call-template name="enddoc"/>
</xsl:template>

<!-- No slide show here ! -->
<xsl:template match="slideshow">
  <xsl:message terminate="yes">
    <xsl:text>ERROR : A slideshow (in file file.xml) must be built with make file_slideshow !</xsl:text>
  </xsl:message>
</xsl:template>

<!-- The page -->
<xsl:template match="page">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:text>\page{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
  <xsl:if test="not(slide) and $slide='true'">
    <xsl:call-template name="diapoauto"/>
  </xsl:if>
  <xsl:apply-templates select="*[not(self::title)]"/>
  </xsl:if>
</xsl:template>

<!-- The <info> header -->
<xsl:template match="info">
  <xsl:text>\titrep{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
  <xsl:text>\pagenotice{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;\sommaire&#10;</xsl:text>
  <xsl:apply-templates select="description"/>
  <xsl:apply-templates select="objectives"/>
</xsl:template>

<!-- A slide -->
<xsl:template match="slide">
  <xsl:if test="$slide='true'">
  <xsl:call-template name="begindiapo"/>
  <xsl:choose>
    <xsl:when test="image">
      <xsl:apply-templates select="image" mode="diapo"/> 
    </xsl:when>
    <xsl:when test="list">
      <xsl:apply-templates select="list" mode="diapo"/> 
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates mode="diapo"/> 
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="enddiapo"/>
  </xsl:if>
</xsl:template>

<!-- A section -->
<xsl:template match="section">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:variable name="level" select="count(ancestor::section)"/>
  <xsl:choose>
    <xsl:when test='$level=0'>
      <xsl:text>\section{</xsl:text>
    </xsl:when>
    <xsl:when test='$level=1'>
      <xsl:text>\subsection{</xsl:text>
    </xsl:when>
    <xsl:when test='$level=2'>
      <xsl:text>\subsubsection{</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:message terminate="yes">
        <xsl:text>ERROR : No sub-sub-sub-section allowed !</xsl:text>
      </xsl:message>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
  <xsl:apply-templates select="*[not(self::title)]"/>
  </xsl:if>
</xsl:template>

<!-- A paragraph -->
<xsl:template match="para">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:choose>
    <xsl:when test="@icon">
      <xsl:text>\marge{</xsl:text>
      <xsl:call-template name="icone"/>
      <xsl:text>}{</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>}&#10;</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>&#10;&#10;</xsl:text>
  </xsl:if>
</xsl:template>

<!-- A note -->
<xsl:template match="note">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:if test="@trainer=$trainer or $trainer='true' or not(@trainer)">
  <xsl:choose>
    <xsl:when test="@icon">
      <xsl:text>\begin{note}[</xsl:text>
      <xsl:call-template name="icone"/>
      <xsl:text>]&#10;</xsl:text>
    </xsl:when>
    <xsl:when test="@trainer='true'">
      <xsl:text>\begin{note}[</xsl:text>
      <xsl:call-template name="icone">
        <xsl:with-param name="nom">trainer</xsl:with-param>
      </xsl:call-template>
      <xsl:text>]&#10;</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>\begin{note}&#10;</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:apply-templates/>
  <xsl:text>&#10;\end{note}&#10;</xsl:text>
  </xsl:if>
  </xsl:if>
</xsl:template>

<!-- An exercise (question only) -->
<xsl:template match="exercise">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:choose>
    <xsl:when test="answer">
      <xsl:text>\begin{question}</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>\begin{questionsansreponse}</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="@icon">
    <xsl:text>[</xsl:text>
    <xsl:call-template name="icone"/>
    <xsl:text>]</xsl:text>
  </xsl:if>
  <xsl:text>&#10;</xsl:text>
  <xsl:apply-templates select="question"/>
  <xsl:choose>
    <xsl:when test="answer">
      <xsl:text>&#10;\end{question}</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>&#10;\end{questionsansreponse}</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  </xsl:if>
</xsl:template>

<!-- An exercice with its solution
     (included at the end of the document) -->
<xsl:template match="exercise" mode="exercice">
  <xsl:if test="@restriction='all' or contains(concat(' ',$selection,' '),concat(' ',@restriction,' ')) or not(@restriction) or $selection='all'">
  <xsl:choose>
    <xsl:when test="answer">
      <xsl:text>&#10;\exercice{</xsl:text>
      <xsl:apply-templates select="title"/>
      <xsl:text>}&#10;</xsl:text>

      <xsl:choose>
	<xsl:when test="@icon">
	  <xsl:text>\marge{</xsl:text>
	  <xsl:call-template name="icone"/>
	  <xsl:text>}{</xsl:text>
	  <xsl:apply-templates select="question"/>
	  <xsl:text>}</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates select="question"/>
	</xsl:otherwise>
      </xsl:choose>

      <xsl:text>\begin{reponse}&#10;</xsl:text>
      <xsl:apply-templates select="answer"/>
      <xsl:text>&#10;\end{reponse}&#10;</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>\exercicesansreponse{}&#10;</xsl:text> 
    </xsl:otherwise>
  </xsl:choose>
  </xsl:if>
</xsl:template>

<!-- An glossary term -->
<xsl:template match="glossary">
      <!-- <xsl:apply-templates/><xsl:text>&#10;\raisebox{.5ex}{\ding{43}}</xsl:text> -->
      <xsl:text>\glossaryterm{</xsl:text><xsl:value-of select="@name"/><xsl:text>}</xsl:text>
</xsl:template>

<!-- An glossary term
     (included at the end of the document) -->
<xsl:template match="glossary" mode="glossary">
  <xsl:if test="text() != ''">
    <xsl:text>\glossarydefinition{</xsl:text>
    <xsl:value-of select="@name"/><xsl:text>}{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:if>
</xsl:template>

<!-- Description and Objectives -->
<xsl:template match="description">
  <xsl:if test="$description='true'">
  <xsl:choose>
    <xsl:when test="normalize-space(.)">
      <xsl:text>{\hbox{\fontsize{13}{20pt}\usefont{\encodingdefault}{phv}{b}{n}\color{LogideeBlue} \Description}</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>&#10;</xsl:text>
    </xsl:when>
  </xsl:choose>
  </xsl:if>
</xsl:template>

<xsl:template match="objectives">
  <xsl:if test="$description='true'">
  <xsl:choose>
    <xsl:when test="normalize-space(item)">
      <xsl:text>\vspace{1em}</xsl:text>
      <xsl:text>{\hbox{\fontsize{13}{20pt}\usefont{\encodingdefault}{phv}{b}{n}\color{LogideeBlue} \Objectives}</xsl:text>
      <xsl:text>\begin{liste}&#10;</xsl:text>
      <xsl:apply-templates/>
      <xsl:text>\end{liste}&#10;</xsl:text>
    </xsl:when>
  </xsl:choose>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
