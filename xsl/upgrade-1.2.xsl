<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output encoding="UTF-8" method="xml" standalone="no" version="1.0"
	    doctype-system="../dtd/module.dtd"
	    doctype-public="-//Logidee//DTD logidee-tools V1.2//EN"
	    cdata-section-elements="code"/>

<xsl:template match="cmd">
  <cmd><xsl:value-of select="@nom"/></cmd>
</xsl:template>

<xsl:template match="menu">
  <menu><xsl:value-of select="@nom"/></menu>
</xsl:template>

<xsl:template match="fichier">
  <file><xsl:value-of select="@nom"/></file>
</xsl:template>

<xsl:template match="liste">
  <list><xsl:apply-templates select="node()|@*"/></list>
</xsl:template>

<xsl:template match="titre">
  <title><xsl:apply-templates select="node()|@*"/></title>
</xsl:template>

<xsl:template match="url">
  <xsl:element name="url">
    <xsl:attribute name="href">
      <xsl:value-of select="@nom"/>
    </xsl:attribute>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template match="objectifs">
  <objectives><xsl:apply-templates select="node()|@*"/></objectives>
</xsl:template>

<xsl:template match="ratio">
  <xsl:element name="ratio">
    <xsl:attribute name="value">
      <xsl:value-of select="@valeur"/>
    </xsl:attribute>
  </xsl:element>
</xsl:template>

<xsl:template match="duree">
  <xsl:element name="duration">
    <xsl:attribute name="value">
      <xsl:value-of select="@valeur"/>
    </xsl:attribute>
    <xsl:attribute name="unit">
      <xsl:value-of select="@unite"/>
    </xsl:attribute>
  </xsl:element>
</xsl:template>

<xsl:template match="prerequis">
  <prerequisite><xsl:apply-templates select="node()|@*"/></prerequisite>
</xsl:template>

<xsl:template match="dependance">
  <dependency><xsl:apply-templates select="node()|@*"/></dependency>
</xsl:template>

<xsl:template match="version">
  <xsl:element name="version">
    <xsl:attribute name="number">
      <xsl:value-of select="@valeur"/>
    </xsl:attribute>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template match="auteur">
  <author><xsl:apply-templates select="node()|@*"/></author>
</xsl:template>

<xsl:template match="commentaire">
  <comment><xsl:apply-templates select="node()|@*"/></comment>
</xsl:template>

<xsl:template match="niveau">
  <xsl:element name="level">
    <xsl:attribute name="value">
      <xsl:value-of select="@valeur"/>
    </xsl:attribute>
  </xsl:element>
</xsl:template>

<xsl:template match="etat">
  <xsl:element name="state">
    <xsl:attribute name="finished">
      <xsl:value-of select="@fini"/>
    </xsl:attribute>
    <xsl:attribute name="proofread">
      <xsl:value-of select="@relu"/>
    </xsl:attribute>
  </xsl:element>
</xsl:template>

<xsl:template match="relecteurs">
  <proofreaders><xsl:apply-templates select="node()|@*"/></proofreaders>
</xsl:template>

<xsl:template match="diaporama">
  <slideshow><xsl:apply-templates select="node()|@*"/></slideshow>
</xsl:template>

<xsl:template match="diapo">
  <xsl:element name="slide">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:element>
</xsl:template>

<xsl:template match="exercice">
  <xsl:element name="exercise">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:element>
</xsl:template>

<xsl:template match="reponse">
  <answer><xsl:apply-templates select="node()|@*"/></answer>
</xsl:template>

<xsl:template match="ligne">
  <row><xsl:apply-templates select="node()|@*"/></row>
</xsl:template>

<xsl:template match="legende">
  <caption><xsl:apply-templates select="node()|@*"/></caption>
</xsl:template>

<!-- Conversion d'attributs -->
<xsl:template match="@truc">
  <xsl:attribute name="icon">
    <xsl:value-of select="."/>
  </xsl:attribute>
</xsl:template>

<xsl:template match="@visible">
  <xsl:attribute name="visible">
    <xsl:choose>
      <xsl:when test="string(.)='oui'">
        <xsl:text>true</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>false</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:attribute>
</xsl:template>

<xsl:template match="@fond">
  <xsl:attribute name="background">
    <xsl:value-of select="."/>
  </xsl:attribute>
</xsl:template>

<xsl:template match="@distrib">
  <xsl:attribute name="restriction">
    <xsl:value-of select="."/>
  </xsl:attribute>
</xsl:template>

<xsl:template match="@formateur">
  <xsl:attribute name="trainer">
    <xsl:value-of select="."/>
  </xsl:attribute>
</xsl:template>

<!-- Generic template to copy the rest -->
<xsl:template match="@*|node()|comment()" priority="-1">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()|comment()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
