#!/bin/bash
# Vérifie les dépendances d'un fichier XML en parcourant les balises
# xi:include

#set -x

function verif() { 
  while [ $# -ne 0 ];
  do
  echo $1
  verif $(sed -e '/<code>/,/<\/code>/ s/.*//' -e 's/.*<\/code>//' -e '/<cmd>/,/<\/cmd>/ s/.*//' -e 's/.*<\/cmd>//' $1 | grep '<xi:include' | sed -e 's/.*href="\([^"]*\)".*/\1/' )
  shift
  done
}

verif  $1
