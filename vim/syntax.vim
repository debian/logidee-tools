" Fichier de syntaxe XML pour Logidée, basé sur le file de Syntaxe XML
" standard de vim et les macros de Marc Simon

"
" Syntaxe
"

syn clear

" Following items are case-sensitive
" Case-insensitive rules can be specified by 'syn case ignore' later
syn case ignore

" mark illegal characters
syn match xmlError "[<>&]"


" tags
syn match   xmlSpecial  contained "\\\d\d\d\|\\."
syn region  xmlString   contained matchgroup=xmlStringQ start=+"+ skip=+\\\\\|\\"+ end=+"+ contains=xmlSpecial,xmlSpecialChar
syn region  xmlString   contained matchgroup=xmlStringQ start=+'+ skip=+\\\\\|\\'+ end=+'+ contains=xmlSpecial,xmlSpecialChar
syn region  xmlEndTag             start=+</+    end=+>+              contains=xmlTagError,xmlLogideeMTagName,xmlLogideeDTagName,xmlLogideeCTagName
syn region  xmlTag                start=+<[^/]+ end=+>+              contains=xmlString,xmlTagError,xmlLogideeMTagName,xmlLogideeArgName,xmlLogideeCTagName,xmlLogideeDTagName
syn match   xmlTagError contained "[^>]<"ms=s+1

" TAGS LOGIDEE
syn keyword xmlLogideeMTagName contained module info page slide section
syn keyword xmlLogideeMTagName contained exercise formation theme

syn keyword xmlLogideeDTagName contained file url menu note image cmd
syn keyword xmlLogideeDTagName contained item em code title legend col
syn keyword xmlLogideeDTagName contained subtitle glossary

syn keyword xmlLogideeCTagName contained question answer 
syn keyword xmlLogideeCTagName contained para list table row
syn keyword xmlLogideeCTagName contained prerequisite dependency suggestion
syn keyword xmlLogideeCTagName contained ref description author comment
syn keyword xmlLogideeCTagName contained email date
syn keyword xmlLogideeCTagName contained ratio duration version level state
syn keyword xmlLogideeCTagName contained objectives include proofreaders

" ARGS LOGIDEE
syn keyword xmlLogideeArgName contained value unit trainer src visible 
syn keyword xmlLogideeArgName contained finished proofread number href
syn keyword xmlLogideeArgName contained restriction icon background head
syn keyword xmlLogideeArgName contained border name

" special characters
syn match   xmlSpecialChar contained "&[^;]*;"
syn match   xmlSpecialChar "&[^;]*;"

syn keyword xmlTodo         contained TODO FIXME XXX

" The real comments (this implements the comments as defined by xml,
" but not all xml pages actually conform to it. Errors are flagged.
syn region  xmlComment                start=+<!+        end=+>+ contains=xmlCommentPart,xmlString,xmlCommentError,xmlTodo
syn match   xmlCommentError contained "[^><!]"
syn region  xmlCommentPart  contained start=+--+        end=+--+

" DTD -- we use dtd.vim here
syn region  xmlDocType matchgroup=xmlDocTypeDecl start="<!DOCTYPE"he=s+2,rs=s+2 end=">" contains=xmlDocTypeKeyword,xmlInlineDTD,xmlString
syn keyword xmlDocTypeKeyword contained DOCTYPE PUBLIC SYSTEM
syn region  xmlInlineDTD contained start="\[" end="]" contains=@xmlDTD
syn include @xmlDTD $VIMRUNTIME/syntax/dtd.vim

" Processing instructions
" This allows "?>" inside strings -- good idea?
syn region  xmlProcessing matchgroup=xmlProcessingDelim start="<?" end="?>" contains=xmlString

" CData sections
syn region  xmlCdata matchgroup=xmlCdataDecl start="<!\[CDATA\[" keepend end="]]>"

" synchronizing (does not always work if a comment includes legal
" xml tags, but doing it right would mean to always start
" at the first line, which is too slow)
syn sync match xmlHighlight groupthere NONE "<[/a-zA-Z]"
syn sync match xmlHighlightSkip "^.*['\"].*$"
syn sync minlines=10

if !exists("did_logidee_xml_syntax_inits")
  let did_logidee_xml_syntax_inits = 1
  " The default methods for highlighting.  Can be overridden later
  "
  hi link xmlTodo                      Todo
  hi link xmlEndTag                    xmlTag
  hi link xmlTag                       Identifier
  hi link xmlSpecialChar               xmlSpecial
  hi link xmlSpecial                   Special
  hi link xmlStringQ                   Special
  hi link xmlString                    Normal
  hi link xmlCommentPart               xmlComment
  hi link xmlComment                   Comment
  hi link xmlCommentError              xmlError
  hi link xmlTagError                  xmlError
  hi link xmlError                     Error

  hi link xmlProcessingDelim           Comment
  hi link xmlProcessing                Type
  hi link xmlCdata                     Normal
  hi link xmlCdataDecl                 Constant
  hi link xmlDocType                   Normal
  hi link xmlDocTypeDecl               Identifier
  hi link xmlDocTypeKeyword            Statement
  hi link xmlInlineDTD                 Identifier

  hi link xmlLogideeMTagName           Constant
  hi link xmlLogideeCTagName           Special
  hi link xmlLogideeDTagName           Statement
  hi link xmlLogideeArgName            Type

endif

let b:current_syntax = "xmllogidee"

" vim: ts=8
